BinaryPaddle
============

A simple pong clone made in [Love2D](https://love2d.org) (LÖVE)


## Building the game ##
1. Zip the contents of the master branch. Make sure that main.lua remains at the root and directory structure remains unchanged.
2. Change the file ending of the .zip file to .love
3. Download and install the love2d installer from https://love2d.org/
4. Double click the .love file to run the game
